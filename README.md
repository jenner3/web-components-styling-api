# Web Components Styling API

Finding ways to setup a styling API for Web Components.

Goals:

* checking CSS variables capabilities
* checking CSS variables capabilities with slots
* checking PART attributes capabilities
* ...others?
