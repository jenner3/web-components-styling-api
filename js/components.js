const newElementTemplate = document.createElement("template");
newElementTemplate.innerHTML = `
	<style type="text/css">
		:root {
			color: blue;
		}

		.a-div {
			color: var(--webc-color, #f44);
		}

		.a-div::after {
			content: var(--webc-text, "unset");
		}

		.styled-slot {
			--webc-color: #4f4;
		}
	</style>
	<div class="a-div">DIV TEXT</div>
`;

class BaseWebComponent extends HTMLElement {
	constructor() {
		super();
	}

	connectedCallback() {
		console.log("connected");
	}

	disconnectedCallback() {
		console.log("connected");
	}

	attributeChangedCallback(name, oldValue, newValue) {
		console.log(`attribute ${name} changed: from ${oldValue} to ${newValue}`);
	}
	
	static get observedAttributes() {
		return [];
	}
}

class VariableSensible extends BaseWebComponent {
	constructor() {
		super();

		let r = this.attachShadow({
			mode: "open"
		});

		r.appendChild(newElementTemplate.content.cloneNode(true));
	}
}

class VariableSensibleNested extends BaseWebComponent {
	constructor() {
		super();

		let r = this.attachShadow({
			mode: "open"
		});

		r.innerHTML = `
			<style type="text/css">
				:root {
					color: green;
				}

				.first {
					--webc-text: "'";
				}
			</style>
			<div>TWO</div>
			<variable-sensible class="first"></variable-sensible>
			<hr>
			<variable-sensible></variable-sensible>
		`;
	}
}

class SlottedComponent extends BaseWebComponent {
	constructor() {
		super();

		let r = this.attachShadow({
			mode: "open"
		});

		r.appendChild(newElementTemplate.content.cloneNode(true));
		
		const slot = document.createElement("slot");
		slot.classList.add("styled-slot");
		slot.name = "attributes";
		slot.addEventListener("slotchange", this.slotChangeHandler.bind(this));

		r.appendChild(slot);
	}

	slotChangeHandler(e) {
		e.target.assignedNodes().forEach(assignedSlot => {
			/*
			assignedSlot.querySelectorAll("input").forEach(input => {
				input.value = Math.random().toString()
			});
			*/

			console.log("slotchange for ", assignedSlot);
		});
	}
}

customElements.define("variable-sensible", VariableSensible);
customElements.define("variable-sensible-nested", VariableSensibleNested);
customElements.define("slotted-component", SlottedComponent);